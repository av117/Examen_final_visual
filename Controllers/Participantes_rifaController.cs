﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Examen_final.Data;
using Examen_final.Models;

namespace Examen_final.Controllers
{
    public class Participantes_rifaController : Controller
    {
        private readonly Examen_finalContext _context;

        public Participantes_rifaController(Examen_finalContext context)
        {
            _context = context;
        }

        // GET: Participantes_rifa
        public async Task<IActionResult> Index()
        {
            return View(await _context.Participantes_rifa.ToListAsync());
        }

        // GET: Participantes_rifa/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participantes_rifa = await _context.Participantes_rifa
                .FirstOrDefaultAsync(m => m.Id == id);
            if (participantes_rifa == null)
            {
                return NotFound();
            }

            return View(participantes_rifa);
        }

        // GET: Participantes_rifa/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Participantes_rifa/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Telefono,Nombre_completo,DPI,Direccion,Email")] Participantes_rifa participantes_rifa)
        {
            if (ModelState.IsValid)
            {
                _context.Add(participantes_rifa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(participantes_rifa);
        }

        // GET: Participantes_rifa/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participantes_rifa = await _context.Participantes_rifa.FindAsync(id);
            if (participantes_rifa == null)
            {
                return NotFound();
            }
            return View(participantes_rifa);
        }

        // POST: Participantes_rifa/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Telefono,Nombre_completo,DPI,Direccion,Email")] Participantes_rifa participantes_rifa)
        {
            if (id != participantes_rifa.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(participantes_rifa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Participantes_rifaExists(participantes_rifa.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(participantes_rifa);
        }

        // GET: Participantes_rifa/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var participantes_rifa = await _context.Participantes_rifa
                .FirstOrDefaultAsync(m => m.Id == id);
            if (participantes_rifa == null)
            {
                return NotFound();
            }

            return View(participantes_rifa);
        }

        // POST: Participantes_rifa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var participantes_rifa = await _context.Participantes_rifa.FindAsync(id);
            _context.Participantes_rifa.Remove(participantes_rifa);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool Participantes_rifaExists(int id)
        {
            return _context.Participantes_rifa.Any(e => e.Id == id);
        }
    }
}
