﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Examen_final.Models;

namespace Examen_final.Data
{
    public class Examen_finalContext : DbContext
    {
        public Examen_finalContext (DbContextOptions<Examen_finalContext> options)
            : base(options)
        {
        }

        public DbSet<Examen_final.Models.Participantes_rifa> Participantes_rifa { get; set; }
    }
}
