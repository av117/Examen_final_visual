﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_final.Models
{
    public class Participantes_rifa
    {
        public int Id { get; set; }
        public string Telefono { get; set; }
        public string Nombre_completo { get; set; }
        public string DPI { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
    }
}
